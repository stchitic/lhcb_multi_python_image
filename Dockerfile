FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

MAINTAINER stefan-gabriel.chitic@cern.ch

# http://bugs.python.org/issue19846
# > At the moment, setting "LANG=C" on a Linux system *fundamentally breaks Python 3*, and that's not OK.
ENV LANG C.UTF-8

RUN mkdir -p /lhcb/python_versions/

RUN yum -y update && \
    yum groupinstall -y development && \
    yum install -y \
    bzip2-devel \
    git \
    hostname \
    openssl \
    openssl-devel \
    sqlite-devel \
    sudo \
    tar \
    wget \
    zlib-dev \
    xz-devel \
    mysql++-devel \
    procmail \
    mysql-connector-python \
    java-1.8.0-openjdk.x86_64 \
    python-twisted-core.x86_64 \
    python-twisted-web.x86_64 \
    python-twisted-words.x86_64 \
    python-setuptools.noarch \
    swig.x86_64

# Elastic search

RUN wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.3.noarch.rpm && rpm -ivh elasticsearch-1.7.3.noarch.rpm

RUN systemctl enable elasticsearch.service


# PYTHON 2.6.6

ENV PYTHON_VERSION 2.6.6

RUN mkdir -p /lhcb/python_versions/$PYTHON_VERSION

RUN set -x \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tgz" -o python.tgz \
    && tar -xC /lhcb/python_versions/$PYTHON_VERSION -f python.tgz \
    && rm python.tgz* \
    && cd /lhcb/python_versions/$PYTHON_VERSION/ \
    && mv -f Python-2.6.6/* ./ \
    && ./configure --prefix=/lhcb/python_versions/$PYTHON_VERSION/ \
    && make -j$(nproc) \
    && make install \
    && ldconfig \
    && curl -SL 'https://bootstrap.pypa.io/get-pip.py' | /lhcb/python_versions/$PYTHON_VERSION/python

# install "virtualenv", since the vast majority of users of this image will want it
RUN wget https://pypi.python.org/packages/5c/79/5dae7494b9f5ed061cff9a8ab8d6e1f02db352f3facf907d9eb614fb80e9/virtualenv-15.0.2.tar.gz \
    && tar -zxvf virtualenv-15.0.2.tar.gz \
    && cd virtualenv-15.0.2/ \
    && /lhcb/python_versions/$PYTHON_VERSION/python setup.py install

RUN mkdir -p /lhcb/virtualenv

# virtual env creation
RUN cd /lhcb/virtualenv \
    && virtualenv 2.6.6 -p /lhcb/python_versions/2.6.6/bin/python2.6 \
    && source 2.6.6/bin/activate \
    && pip install nose coverage

# custom librareis
RUN cd /lhcb/virtualenv \
    && source 2.6.6/bin/activate \
    && pip install SQLAlchemy backports.lzma


# PYTHON 2.7.5

ENV PYTHON_VERSION 2.7.5

ENV PYTHON_PIP_VERSION 7.1.2

RUN mkdir -p /lhcb/python_versions/$PYTHON_VERSION

RUN set -x \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz" -o python.tar.xz \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz.asc" -o python.tar.xz.asc \
    && tar -xJC /lhcb/python_versions/$PYTHON_VERSION --strip-components=1 -f python.tar.xz \
    && rm python.tar.xz* \
    && cd /lhcb/python_versions/$PYTHON_VERSION \
    && ./configure --prefix=/lhcb/python_versions/$PYTHON_VERSION/ \
    && make -j$(nproc) \
    && make install \
    && ldconfig \
    && curl -SL 'https://bootstrap.pypa.io/get-pip.py' | /lhcb/python_versions/$PYTHON_VERSION/python

# install "virtualenv", since the vast majority of users of this image will want it
RUN wget https://pypi.python.org/packages/5c/79/5dae7494b9f5ed061cff9a8ab8d6e1f02db352f3facf907d9eb614fb80e9/virtualenv-15.0.2.tar.gz \
    && tar -zxvf virtualenv-15.0.2.tar.gz \
    && cd virtualenv-15.0.2/ \
    && /lhcb/python_versions/$PYTHON_VERSION/python setup.py install


# virtual env creation
RUN cd /lhcb/virtualenv \
    && virtualenv 2.7.5 -p /lhcb/python_versions/2.7.5/bin/python2.7 \
    && source 2.7.5/bin/activate \
    && pip install nose coverage

# custom librareis
RUN cd /lhcb/virtualenv \
    && source 2.7.5/bin/activate \
    && pip install SQLAlchemy backports.lzma

# PYTHON 2.7.12

ENV PYTHON_VERSION 2.7.12

ENV PYTHON_PIP_VERSION 7.1.2

RUN mkdir -p /lhcb/python_versions/$PYTHON_VERSION

RUN set -x \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz" -o python.tar.xz \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz.asc" -o python.tar.xz.asc \
    && tar -xJC /lhcb/python_versions/$PYTHON_VERSION --strip-components=1 -f python.tar.xz \
    && rm python.tar.xz* \
    && cd /lhcb/python_versions/$PYTHON_VERSION \
    && ./configure --prefix=/lhcb/python_versions/$PYTHON_VERSION/ \
    && make -j$(nproc) \
    && make install \
    && ldconfig \
    && curl -SL 'https://bootstrap.pypa.io/get-pip.py' | /lhcb/python_versions/$PYTHON_VERSION/python


# install "virtualenv", since the vast majority of users of this image will want it
RUN wget https://pypi.python.org/packages/5c/79/5dae7494b9f5ed061cff9a8ab8d6e1f02db352f3facf907d9eb614fb80e9/virtualenv-15.0.2.tar.gz \
    && tar -zxvf virtualenv-15.0.2.tar.gz \
    && cd virtualenv-15.0.2/ \
    && /lhcb/python_versions/$PYTHON_VERSION/python setup.py install

# virtual env creation
RUN cd /lhcb/virtualenv \
    && virtualenv 2.7.12 -p /lhcb/python_versions/2.7.12/bin/python2.7 \
    && source 2.7.12/bin/activate \
    && pip install nose coverage

# custom librareis
RUN cd /lhcb/virtualenv \
    && source 2.7.12/bin/activate \
    && pip install SQLAlchemy backports.lzma


# PYTHON 3.5.2

ENV PYTHON_VERSION 3.5.2

ENV PYTHON_PIP_VERSION 7.1.2

ENV LC_ALL en_US.UTF-8

RUN mkdir -p /lhcb/python_versions/$PYTHON_VERSION

RUN set -x \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz" -o python.tar.xz \
    && curl -SL "https://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz.asc" -o python.tar.xz.asc \
    && tar -xJC /lhcb/python_versions/$PYTHON_VERSION --strip-components=1 -f python.tar.xz \
    && rm python.tar.xz* \
    && cd /lhcb/python_versions/$PYTHON_VERSION \
    && ./configure --prefix=/lhcb/python_versions/$PYTHON_VERSION/ \
    && make -j$(nproc) \
    && make install \
    && ldconfig \
    && curl -SL 'https://bootstrap.pypa.io/get-pip.py' | /lhcb/python_versions/$PYTHON_VERSION/python


# install "virtualenv", since the vast majority of users of this image will want it
RUN wget https://pypi.python.org/packages/8b/2c/c0d3e47709d0458816167002e1aa3d64d03bdeb2a9d57c5bd18448fd24cd/virtualenv-15.0.3.tar.gz \
    && tar -zxvf virtualenv-15.0.3.tar.gz \
    && cd virtualenv-15.0.3/ \
    && /lhcb/python_versions/$PYTHON_VERSION/python setup.py install

# virtual env creation
RUN cd /lhcb/virtualenv \
    && /lhcb/python_versions/$PYTHON_VERSION/python -m virtualenv 3.5.2 -p /lhcb/python_versions/3.5.2/bin/python3.5 \
    && source 3.5.2/bin/activate \
    && pip install nose coverage

# custom librareis
RUN cd /lhcb/virtualenv \
    && source 3.5.2/bin/activate \
    && pip install SQLAlchemy backports.lzma


#CHANGE access for all paths
RUN chmod -R a+w /lhcb/virtualenv \
    && chmod -R g+w /lhcb/virtualenv 

RUN yum -y update && \
    yum groupinstall -y development && \
    yum install -y \
        ncurses-devel \
        readline-devel


